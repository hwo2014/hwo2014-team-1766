package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import net.sourceforge.jFuzzyLogic.FIS;

import com.google.gson.Gson;


public class Main {
    public static void main(String... args) throws IOException {
      
    	String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
       
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }
    
    final Gson gson = new Gson();
    
    private PrintWriter writer;
    public Track track;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        FIS fis = FIS.load("src/main/java/noobbot/fuzzy.fcl", true);
        if (fis == null) {
    		System.err.println("Can't load file: fuzzy.fcl");
    		return;
    	}
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	CarWrapper carWrapper = gson.fromJson(line, CarWrapper.class);
            	List<CarPositions> carPositionList = carWrapper.getData();
            	CarPositions myPosition = getId(carPositionList);
            	int pieceIndex = myPosition.getPosition().getIndex();
            	Piece currentPiece = getCurrentPiece(pieceIndex);
            	fis.setVariable("angle", myPosition.getSlip());
            	fis.setVariable("length", currentPiece.length);
            	fis.setVariable("radius", currentPiece.radius);
            	double throttle = fis.getVariable("throttle").getLatestDefuzzifiedValue()/100;
            	System.out.println(throttle);
                send(new Throttle(throttle));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	GameInitWrapper gameWrapper = gson.fromJson(line, GameInitWrapper.class);
            	RaceData raceData = gameWrapper.getData();
            	track = raceData.getRace().getTrack();
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private Piece getCurrentPiece(int pieceIndex) {
		return track.getPieceList().get(pieceIndex);
	}

	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }


public CarPositions getId(List<CarPositions> positionList){
	for(CarPositions carPosition : positionList){
		if(carPosition.getId().getName().equals("PandaBearSoup")){
			return carPosition;
		}
	}
	return null;
}

}





   





