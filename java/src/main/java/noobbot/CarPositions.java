package noobbot;

public class CarPositions {
	double angle;
	PiecePosition piecePosition;
	int lap;
	Id id;

	public double getSlip() {
		return angle;
	}

	public PiecePosition getPosition() {
		return piecePosition;
	}

	public int getLap() {
		return lap;
	}

	public Id getId() {
		return id;
	}
}
